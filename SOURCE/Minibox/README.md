# Minibox

Small Linux commands for resource limited systems

Copyright (C) 2017-2021 Ercan Ersoy

This software is licensed under GNU General Public License version 2 and GNU General Public License version 3.

## Thanks

Thanks to Berki Yenigün for French translations of strings.

## Compile

The program must be compiled with GCC.

### MS-DOS, FreeDOS, etc.

You must use DJGPP.

You should be compiled this command line is given below.

    make dos

## Usage

    MINIBOX Command [Arguments]
    MINIBOX --help|--version

## Commands

* beep
* cat
* cd
* clear
* cp
* date
* echo
* help
* ls
* mkdir
* mv
* pwd
* rm
* rmdir
* sh
* time

## Notes

* FreeDOS is a registered trademark of Jim Hall.
* Linux is a registered trademark of Linus Tovarlds.
* MS-DOS is a registered trademark of Microsoft Corporation.
